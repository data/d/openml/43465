# OpenML dataset: Boston-house-price-data

https://www.openml.org/d/43465

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset is extracted from the The Boston Housing Dataset, and the extraction of the data is explained in Extract dataset/dataframe from an URL
Acknowledgements
A Dataset derived from information collected by the U.S. Census Service concerning housing in the area of Boston Mass.
Column description:
This dataset contains information collected by the U.S Census Service concerning housing in the area of Boston Mass. It was obtained from the StatLib archive (http://lib.stat.cmu.edu/datasets/boston), and has been used extensively throughout the literature to benchmark algorithms. However, these comparisons were primarily done outside of Delve and are thus somewhat suspect. The dataset is small in size with only 506 cases.
The data was originally published by Harrison, D. and Rubinfeld, D.L. Hedonic prices and the demand for clean air', J. Environ. Economics  Management, vol.5, 81-102, 1978.
Variables in order:
 CRIM     per capita crime rate by town
 ZN       proportion of residential land zoned for lots over 25,000 sq.ft.
 INDUS    proportion of non-retail business acres per town
 CHAS     Charles River dummy variable (= 1 if tract bounds river; 0 otherwise)
 NOX      nitric oxides concentration (parts per 10 million)
 RM       average number of rooms per dwelling
 AGE      proportion of owner-occupied units built prior to 1940
 DIS      weighted distances to five Boston employment centres
 RAD      index of accessibility to radial highways
 TAX      full-value property-tax rate per 10,000
 PTRATIO  pupil-teacher ratio by town
 B        1000(Bk - 0.63)2 where Bk is the proportion of blacks by town
 LSTAT     lower status of the population
 MEDV     Median value of owner-occupied homes in 1000's

Inspiration
I'd like to find it as the base for data exploration in regression way

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43465) of an [OpenML dataset](https://www.openml.org/d/43465). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43465/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43465/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43465/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

